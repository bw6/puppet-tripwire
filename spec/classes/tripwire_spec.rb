require 'spec_helper'

describe 'tripwire' do
  on_supported_os.each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }
      let(:params) do
        {
          'tw_local_pass' => 'SomeLocalPass',
          'tw_site_pass'  => 'SomeSecretSitePass',
        }
      end

      it { is_expected.to compile }
    end
  end
end
