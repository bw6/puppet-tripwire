# Changelog

All notable changes to this project will be documented in this file.

## Release 0.2.1

**Features**

**Bugfixes**

* Fixed functionality to skip directories.

**Known Issues**

## Release 0.2.0

**Features**

* Add the ability to skip directories when generating the policy.

**Bugfixes**

* Skipped directories only worked one level deep

**Known Issues**

## Release 0.1.0

**Features**

* Initial release

**Bugfixes**

**Known Issues**
