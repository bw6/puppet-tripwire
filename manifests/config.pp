# Configure tripwire and its policy
#
# @summary Configure tripwire
#
# @api private
#
# @param config_file
#   The location of the config file on the filesystem.
#
# @param crit_dirs
#   The directories that `gen_twpol.py` will scan to build the policy.
#
# @param globalemail
#   List of email addresses separated by either a comma `,`, or semi-colon `;`.
#   If a report would have normally been sent out, it will also be sent to this
#   list of recipients.
#
# @param localkeyfile
#   The location of the local key file on the filesystem.
#
# @param mailnoviolations
#   This option controls the way that Tripwire sends email notification if no
#   rule violations are found during an integrity check.  If `mailnoviolations`
#   is set to false and no violations are found, Tripwire will not send a
#   report. With any other value, or if the variable is removed from the
#   configuration file, Tripwire will send an email message stating that no
#   violations were found.
#
# @param polfile
#   The location of the compiled policy on the filesystem.
#
# @param skip_dirs
#   The directories that `gen_twpol.py` will skip over when scanning to build
#   the policy.
#
# @param sitekeyfile
#   The location of the site key file on the filesystem.
#
# @param tw_local_pass
#   The local passphrase used by tripwire
#
# @param tw_site_pass
#   The site passphrase used by tripwire
#
# @param txt_cfg
#   The loation of the plain text config file on the filesystem.
#
# @param txt_pol
#   The location of the plain text policy on the filesystem.
#
# @param txt_pol_header
#   The location of the plain text header `gen_twpol.py` will use when building
#   the policy.
#
# @param varlog_monitor
#   Should tripwire monitor the contents of `/var/log` for changes?
#
# @param varlog_prop_mask
#   The property mask for monitoring log files.
#
# @param varlog_severity
#   Severity level for the `/var/log` rule.
#
class tripwire::config (
  String                $config_file      = $::tripwire::config_file,
  Array                 $crit_dirs        = $::tripwire::crit_dirs,
  Optional[String]      $globalemail      = $::tripwire::globalemail,
  String                $localkeyfile     = $::tripwire::localkeyfile,
  Enum['false', 'true'] $mailnoviolations = $::tripwire::mailnoviolations, # lint:ignore:quoted_booleans
  String                $polfile          = $::tripwire::polfile,
  String                $sitekeyfile      = $::tripwire::sitekeyfile,
  Array                 $skip_dirs        = $::tripwire::skip_dirs,
  String                $tw_local_pass    = $::tripwire::tw_local_pass,
  String                $tw_site_pass     = $::tripwire::tw_site_pass,
  String                $txt_cfg          = $::tripwire::txt_cfg,
  String                $txt_pol          = $::tripwire::txt_pol,
  String                $txt_pol_header   = $::tripwire::txt_pol_header,
  Boolean               $varlog_monitor   = $::tripwire::varlog_monitor,
  String                $varlog_prop_mask = $::tripwire::varlog_prop_mask,
  Variant[Enum['$(SIG_HI)', '$(SIG_MED)', '$(SIG_LOW)'], Integer[0, 1000000]]
                        $varlog_severity  = $::tripwire::varlog_severity,
) inherits tripwire {

  exec { 'tw_create_site_key':
    command => "/usr/sbin/twadmin --generate-keys --site-keyfile ${sitekeyfile} --site-passphrase '${tw_site_pass}'",
    creates => $sitekeyfile,
  }

  file { $sitekeyfile:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    require => Exec['tw_create_site_key'],
  }

  exec { 'tw_create_local_key':
    command => "/usr/sbin/twadmin --generate-keys --local-keyfile ${localkeyfile} --local-passphrase '${tw_local_pass}'",
    creates => $localkeyfile,
  }

  file { $localkeyfile:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    require => Exec['tw_create_local_key'],
  }

  file { $txt_cfg:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    content => epp('tripwire/twcfg.txt.epp'),
    notify  => Exec['sign_tw_config_file'],
  }

  exec { 'sign_tw_config_file':
    command     => "/usr/sbin/twadmin --create-cfgfile --site-keyfile ${sitekeyfile} --site-passphrase '${tw_site_pass}' ${txt_cfg}",
    refreshonly => true,
    require     => File[$sitekeyfile],
    notify      => Exec['initialize_tw_database'],
  }

  file { $config_file:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    require => Exec['sign_tw_config_file'],
  }

  file { $txt_pol_header:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    content => epp('tripwire/twpol_header.txt.epp'),
    notify  => Exec['generate_policy'],
  }

  file { '/etc/tripwire/gen_twpol.py':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0750',
    content => epp('tripwire/gen_twpol.py.epp'),
    notify  => Exec['generate_policy'],
  }

  exec { 'generate_policy':
    command     => '/etc/tripwire/gen_twpol.py',
    cwd         => '/etc/tripwire',
    refreshonly => true,
    notify      => Exec['sign_tw_policy_file'],
  }

  file { $txt_pol:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    require => Exec['generate_policy'],
  }

  exec { 'sign_tw_policy_file':
    command     => "/usr/sbin/twadmin --create-polfile --site-keyfile ${sitekeyfile} --site-passphrase '${tw_site_pass}' ${txt_pol}",
    refreshonly => true,
    require     => File[$config_file, $txt_pol],
    notify      => Exec['initialize_tw_database'],
  }

  file { $polfile:
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0640',
    require => Exec['sign_tw_policy_file'],
  }

  exec { 'initialize_tw_database':
    command     => "/usr/sbin/tripwire --init --local-passphrase '${tw_local_pass}'",
    refreshonly => true,
    require     => File[$polfile],
  }
}
